import React from 'react';
import styled from "styled-components";

const EmAltaDiv = styled.div`
    padding: 1.3rem 1.5rem;
    :hover{
        background-color: gainsboro;
        cursor: pointer;
    }
`;

const EmAltaTituloSecao = styled.h3`
    position: relative;
    right: 0.8rem;
`;

const EmAltaTitulo = styled.h3`
    font-size: 1.2rem;
    text-decoration: underline;
`;

const EmAltaParagrafo = styled.h3`
    font-size: 0.8rem;
`;

function EmAltaItem({ title, text, isTitle = false }) {
    if (isTitle) {
        return (
            <>
                <EmAltaDiv>
                    <EmAltaTituloSecao>{title}</EmAltaTituloSecao>
                </EmAltaDiv>
                <hr></hr>
            </>
        );
    } else {
        return (
            <>
                <EmAltaDiv>
                    <EmAltaTitulo>{title}</EmAltaTitulo>
                    <EmAltaParagrafo>{text}</EmAltaParagrafo>
                </EmAltaDiv>
                <hr></hr>
            </>
        );
    }
}

export default EmAltaItem;
import React from "react";
import styled from "styled-components";

const NoticiasDiv = styled.div`
    background-color: white;
    height: 30vh;
    width: 35vw;
    display: flex;

    :hover{
        background-color: gainsboro;
        cursor: pointer;
    }
`;

const NoticiasNav = styled.h3`
    margin: 1rem;
    color: rgb(99, 99, 163);
`;

const NoticiasTitle = styled.h3`
    margin: 1rem;
`;

const NoticiasText = styled.p`
    margin: 1rem;
    font-size: 0.8rem;
`;

const NoticiasImg = styled.img`
    position: relative;
    height: 30vh;
    width: 12vw;
`;

function NoticiasItem({ type, title, text, img = "null" }) {
    if (img === "null") {
        return (
            <>
                <NoticiasDiv>
                    <div>
                        <NoticiasNav>{type}</NoticiasNav>
                        <NoticiasTitle>{title}</NoticiasTitle>
                        <NoticiasText>{text}</NoticiasText>
                    </div>
                </NoticiasDiv>
            </>
        );
    } else {
        return (
            <>
                <NoticiasDiv>
                    <div>
                        <NoticiasImg src={img} alt=""></NoticiasImg>
                    </div>
                    <div>
                        <NoticiasNav>{type}</NoticiasNav>
                        <NoticiasTitle>{title}</NoticiasTitle>
                        <NoticiasText>{text}</NoticiasText>
                    </div>
                </NoticiasDiv>
            </>
        );
    }
}

export default NoticiasItem;
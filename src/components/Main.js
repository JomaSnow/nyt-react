import React from "react";
import styled from "styled-components";
import EmAlta from "./EmAlta";
import Noticias from "./Noticias";

const MainBody = styled.main`
    position: relative;
    top: 9rem;
    background-color: lightgray;
    width: 100%;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-around;
    align-items: center;
`;

function Main(){
    return (
        <>
            <MainBody>
                <EmAlta></EmAlta>
                <Noticias></Noticias>
            </MainBody>
        </>
    );
}

export default Main;
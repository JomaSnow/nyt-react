import React from 'react';
import styled from "styled-components";

const Header = styled.header`
    position: fixed;
    top: 0;
    width: 100%;
    box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.5);
    z-index: 2;
`;

const TitleArea = styled.section`
    background-color: lightblue;
    width: 100%;
    padding: 0.5rem;
    text-align: center;
`;

const Title = styled.h1`
    font-size: 3rem;
    font-family: cursive;
`;

const SubTitle = styled.h2`
    font-size: 1rem;
    position: relative;
    bottom: 1rem;
`;

const NavLinkHeader = styled.section`
    background-color: midnightblue;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-around;
    align-items: center;
    align-content: center;
    padding: 0 4rem;
`;

const NavLink = styled.a`
    color: white;
    text-decoration: none;
    font-weight: bold;
    padding: 0.5rem 0;

    :hover{
        background-color: rgb(99, 99, 163);
    color: white;
    }
`;

//para utilizar funcoes como html componente precisa ser maiusculo
function Banner() {
    return (
        <>
            <Header>
                <TitleArea>
                    <Title>The New Yamin Times</Title>
                    <SubTitle>Notícias de verdade para quem tem compromisso com a verdade.</SubTitle>
                </TitleArea>
                <NavLinkHeader>
                    <NavLink href="">Mundo</NavLink>
                    <NavLink href="">Brasil</NavLink>
                    <NavLink href="">Tecnologia</NavLink>
                    <NavLink href="">Política</NavLink>
                    <NavLink href="">Estilo de vida</NavLink>
                    <NavLink href="">Negócios</NavLink>
                    <NavLink href="">Esportes</NavLink>
                    <NavLink href="">Arte</NavLink>
                </NavLinkHeader>
            </Header>
        </>
    );
}

export default Banner;
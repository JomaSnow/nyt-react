import React from "react";
import styled from "styled-components";
import NoticiasItem from "./NoticiasItem";

const NoticiasRowDiv = styled.div`
    margin-top: 1rem;
    display: flex;
    justify-content: space-around;
`;

function NoticiasRow({noticia1, noticia2}){
    return(
        <>
            <NoticiasRowDiv>
                <NoticiasItem img={noticia1.img} type={noticia1.type} title={noticia1.title} text={noticia1.text}></NoticiasItem>
                <NoticiasItem img={noticia2.img} type={noticia2.type} title={noticia2.title} text={noticia2.text}></NoticiasItem>
            </NoticiasRowDiv>
        </>
    );
}

export default NoticiasRow;
import React from 'react';
import styled from "styled-components";
import EmAltaItem from "./EmAltaItem";

const EmAltaSection = styled.section`
    width: 20vw;
    background-color: white;
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    justify-content: flex-start;
    align-content: center;
`;

function EmAlta() {
    return (
        <>
        <EmAltaSection>
            <EmAltaItem title="Em Alta" isTitle={true}></EmAltaItem>
            <EmAltaItem title="Looks no Brexit" text="A saída do Reino Unido da União Europeia trouxe outfits ousados para homens 60+, e os melhores looks você encontra aqui."></EmAltaItem>
            <EmAltaItem title="iOS 14: Adeus teclado, olá figurinhas" text="Em uma estratégia ousada, Apple se livra do teclado tradicinal a favor da comunicação exclusiva com figurinhas na nova Beta do iOS 14."></EmAltaItem>
            <EmAltaItem title="Entrevista com CEO da NYT: o homem por trás do CSS" text='João Lucas Yamin, o mais manjador de CSS na CJR, revela seus segredos e técnicas para programação de layouts com HTML e CSS: "Eu não decorei todo o CSS. Só uns 80%". Confira a entrevista na íntegra aqui.'></EmAltaItem>
            <EmAltaItem title="Chocotone - a época mais gostosa do ano se aproxima" text="Em preparação para o Natal, Chocotones começam a ser produzidos para venda em massa. Em postagem em rede social, Bauduco explica por que ainda insiste em fabricar Panetone, o primo menos sucedido do Chocotone. Veja as reações."></EmAltaItem>
        </EmAltaSection>
        </>
    );
}

export default EmAlta;